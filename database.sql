create database image_data;
use image_data;
CREATE TABLE IF NOT EXISTS image_data (
  id VARCHAR(36) NOT NULL,
  width INT,
  height INT,
  transformation VARCHAR(30),
  location VARCHAR(255),
  original_location VARCHAR(255),
  edited TINYINT(1) DEFAULT 0,
  PRIMARY KEY (id)
);