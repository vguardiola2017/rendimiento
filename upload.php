<?php
require 'autoload.php';

use Viguamu\Performance\Application\PublishMessageToQueueUseCase;
use Viguamu\Performance\Infrastructure\Concurrency\RabbitMQConcurrencyManager;

$target_dir = "uploads/";
$transformed_dir = "transformedUploads/";
$targetDir = $target_dir . str_replace(' ', '_', $_FILES['file']['name']);

if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetDir)) { $status = 1; }

$concurrencyManager = new RabbitMQConcurrencyManager();
$publishMessageToQueueUseCase = new PublishMessageToQueueUseCase($concurrencyManager);

$transformations = [
    ['transformation' => 'resizeToHeight500', 'tag' => 'resizeToHeight500'],
    ['transformation' => 'resizeToHeight200', 'tag' => 'resizeToHeight200'],
    ['transformation' => 'resizeToWidth500', 'tag' => 'resizeToWidth500'],
    ['transformation' => 'resizeToWidth200', 'tag' => 'resizeToWidth200'],
    ['transformation' => 'scaleImage50', 'tag' => 'scaleImage50'],
    ['transformation' => 'gaussian_blur', 'tag' => 'gaussian_blur'],
    ['transformation' => 'cropImage200_200', 'tag' => 'cropped200_200']
];

foreach ($transformations as $transformation) {
    $targetTransformedDir = $transformed_dir . $transformation['tag'] . '_' . str_replace(' ', '-', $_FILES['file']['name']);
    $data = [
        'original_location' => $targetDir,
        'transformation' => $transformation['transformation'],
        'destination_location' => $targetTransformedDir
    ];
    $publishMessageToQueueUseCase('transformations', $data);
}



