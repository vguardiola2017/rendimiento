<?php
include('autoload.php');

$cacheManager = new \Viguamu\Performance\Infrastructure\Database\RedisImageCharacteristicsRepository();
$elasticManager = new \Viguamu\Performance\Infrastructure\Database\ElasticImageDescriptionsRepository();
$mysqlManager = new \Viguamu\Performance\Infrastructure\Database\MySqlImageCharacteristicsRepository();
$searchEditedImages = new \Viguamu\Performance\Application\SearchEditedImagesUseCase($cacheManager, $elasticManager, $mysqlManager);

$imagesFound = $searchEditedImages($_POST['texto']);

foreach ($imagesFound as $imageFound){
    echo '<div class="col-lg-4 col-md-4 mb-4">';
    echo '<div class="card h-100">';
    echo '<img class="card-img-top" src="' . $imageFound['location'] . '">';
    echo '<div class="card-body">';
    echo '<h4 class="card-title">' . $imageFound['name'] . '</h4>';
    echo '<p style="font-size: 18px;"><strong>Width:</strong> ' . $imageFound['width'] . 'px <strong>Height:</strong> ' . $imageFound['height'] . 'px</p>';
    echo '<p style="font-size: 18px;"><strong>Transformation:</strong> ' . $imageFound['transformation'] . '</p>';
    echo '<p style="font-size: 18px;"><strong>Tags:</strong> ' . $imageFound['tags'] . '</p>';
    echo '<p style="font-size: 18px;"><strong>Description:</strong> ' . $imageFound['description'] . '</p>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}