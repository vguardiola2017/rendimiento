<?php
require 'autoload.php';

use Viguamu\Performance\Application\ListUneditedImagesUseCase;
use Viguamu\Performance\Infrastructure\Database\MySqlImageCharacteristicsRepository;

$imagesRepository = new MySqlImageCharacteristicsRepository();
$listUneditedImagesUseCase = new ListUneditedImagesUseCase($imagesRepository);

$uneditedImages = $listUneditedImagesUseCase();

$imagesToRender = [];
foreach ($uneditedImages as $uneditedImage){
    $auxImage = [];
    $auxImage['id'] = $uneditedImage['id'];
    $auxImage['name'] = explode('/', $uneditedImage['location'])[1];
    $auxImage['width'] = $uneditedImage['width'];
    $auxImage['height'] = $uneditedImage['height'];
    $auxImage['original_location'] = $uneditedImage['original_location'];
    $auxImage['transformation'] = $uneditedImage['transformation'];
    $auxImage['location'] = $uneditedImage['location'];

    $imagesToRender[] = $auxImage;
}
echo $twig->render('index.twig', [
    'images' => $imagesToRender
]);