<?php

use Viguamu\Performance\Application\SaveEditedImageDataUseCase;
use Viguamu\Performance\Infrastructure\Database\ElasticImageDescriptionsRepository;
use Viguamu\Performance\Infrastructure\Database\MySqlImageCharacteristicsRepository;
use Viguamu\Performance\Infrastructure\Database\RedisImageCharacteristicsRepository;

require 'autoload.php';

if ($_POST) {
    if (isset($_POST['edit-completed'])) {
        $imageUuid = $_POST['id'];
        $imageName = $_POST['name'];
        $imageWidth = $_POST['width'];
        $imageHeight= $_POST['height'];
        $imageTags = $_POST['tags'];
        $imageDescription = $_POST['description'];
        $imageLocation = $_POST['imageLocation'];

        $mysqlRepository = new MySqlImageCharacteristicsRepository();
        $elasticRepository = new ElasticImageDescriptionsRepository();
        $redisRepository = new RedisImageCharacteristicsRepository();
        $saveEditedImageDataUseCase = new SaveEditedImageDataUseCase($mysqlRepository, $redisRepository, $elasticRepository);

        $imageData['tags'] = $imageTags;
        $imageData['description'] = $imageDescription;
        $imageData['id'] = $imageUuid;

        $saveEditedImageDataUseCase($imageData);

        header('Location: '. 'http://192.168.33.50/index.php');
    }

    $imageUuid = $_POST['id'];
    $imageName = $_POST['name'];
    $imageWidth = $_POST['width'];
    $imageHeight= $_POST['height'];
    $imageTags = $_POST['tags'];
    $imageDescription = $_POST['description'];
    $imageLocation = $_POST['imageLocation'];

    try{
        echo $twig->render('edit-image.twig',
                           [
                               'imageUuid' => $imageUuid,
                               'imageName' => $imageName,
                               'imageWidth' => $imageWidth,
                               'imageHeight' => $imageHeight,
                               'imageTags' => $imageTags,
                               'imageDescription' => $imageDescription,
                               'imageLocation' => $imageLocation
                           ]
        );
    } catch (Exception $e) {
    }
}