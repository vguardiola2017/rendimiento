<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Domain\Repository;

use Viguamu\Performance\Domain\Model\ImageDescriptions;

interface ImageDescriptionsRepository
{
    public function save(ImageDescriptions $image): void;

    public function fullTextSearch(string $textToSearch): array;
}