<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Domain\Repository;

use Viguamu\Performance\Domain\Model\ImageCharacteristics;

interface ImageCharacteristicsRepository
{
    public function save(ImageCharacteristics $image): void;

    public function getUneditedImages(): array;

    public function markImageAsEdited(string $id): void;

    public function getImageCharacteristicsById(string $id): ImageCharacteristics;
}