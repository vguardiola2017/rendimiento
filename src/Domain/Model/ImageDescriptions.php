<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Domain\Model;

final class ImageDescriptions
{
    private $id;
    private $tags;
    private $description;

    public function __construct(string $id, string $tags, string $description)
    {
        $this->id          = $id;
        $this->tags        = $tags;
        $this->description = $description;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTags(): string
    {
        return $this->tags;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}