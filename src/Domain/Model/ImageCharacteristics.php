<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Domain\Model;

use Viguamu\Performance\Infraestructure\Uuid\RamseyUuidGenerator;

final class ImageCharacteristics
{
    private $id;
    private $width;
    private $height;
    private $transformation;
    private $location;
    private $originalImageLocation;

    public function __construct(string $id, int $width, int $height, string $transformation, string $location, string $originalImageLocation)
    {
        $this->id                    = $id;
        $this->width                 = $width;
        $this->height                = $height;
        $this->transformation        = $transformation;
        $this->location              = $location;
        $this->originalImageLocation = $originalImageLocation;
    }

    public static function generateNewImageCharacteristics(int $width, int $height, string $transformation, string $location, string $originalImageLocation)
    {
        $uuidGenerator = new RamseyUuidGenerator();
        $randomId = $uuidGenerator->generateUuid();
        return new ImageCharacteristics($randomId, $width, $height, $transformation, $location, $originalImageLocation);

    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getTransformation(): string
    {
        return $this->transformation;
    }

    public function getLocation(): string
    {
        return $this->location;
    }

    public function getOriginalImageLocation(): string
    {
        return $this->originalImageLocation;
    }
}