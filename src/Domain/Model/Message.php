<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Domain\Model;

final class Message
{
    private $originalLocation;
    private $destinationLocation;
    private $transformation;

    public function __construct(string $rawMessage)
    {
        $message = json_decode($rawMessage, true);
        $this->originalLocation = $message['original_location'];
        $this->destinationLocation = $message['destination_location'];
        $this->transformation = $message['transformation'];
    }

    public function getOriginalLocation(): string
    {
        return $this->originalLocation;
    }

    public function getDestinationLocation(): string
    {
        return $this->destinationLocation;
    }

    public function getTransformation(): string
    {
        return $this->transformation;
    }
}