<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Domain\Uuid;

interface UuidGenerator
{
    public function generateUuid(): string;
}