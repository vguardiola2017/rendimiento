<?php

namespace Viguamu\Performance\Domain\Concurrency;

interface ConcurrencyManager
{
    public function publishMessage($queue, $message): void;

}