<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Application;

use Viguamu\Performance\Infrastructure\Database\MySqlImageCharacteristicsRepository;
use Viguamu\Performance\Infrastructure\Database\RedisImageCharacteristicsRepository;

final class ListUneditedImagesUseCase
{
    private $repository;

    public function __construct(MySqlImageCharacteristicsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(): array
    {
        return $this->repository->getUneditedImages();
    }
}