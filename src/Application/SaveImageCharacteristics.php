<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Application;

use Viguamu\Performance\Domain\Model\ImageCharacteristics;
use Viguamu\Performance\Domain\Repository\ImageCharacteristicsRepository;
use Viguamu\Performance\Domain\Uuid\UuidGenerator;

final class SaveImageCharacteristics
{
    private $repository;
    private $uuidGenerator;
    private $cacheRepository;

    public function __construct(ImageCharacteristicsRepository $repository, ImageCharacteristicsRepository $cacheRepository, UuidGenerator $uuidGenerator)
    {
        $this->repository = $repository;
        $this->uuidGenerator = $uuidGenerator;
        $this->cacheRepository = $cacheRepository;
    }

    public function __invoke(ImageCharacteristics $imageData): string
    {
        $this->repository->save($imageData);
        $this->cacheRepository->save($imageData);
        return $imageData->getId();
    }
}