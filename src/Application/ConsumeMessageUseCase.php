<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Application;

use Viguamu\Performance\Domain\ImageTransformer\ImageTransformer;
use Viguamu\Performance\Domain\Model\ImageCharacteristics;
use Viguamu\Performance\Domain\Model\Message;
use Viguamu\Performance\Infraestructure\Uuid\RamseyUuidGenerator;
use Viguamu\Performance\Infrastructure\Database\MySqlImageCharacteristicsRepository;
use Viguamu\Performance\Infrastructure\Database\RedisImageCharacteristicsRepository;

final class ConsumeMessageUseCase
{
    private $imageTransformer;
    private $saveImageCharacteristicsUseCase;

    public function __construct(ImageTransformer $imageTransformer)
    {
        $this->imageTransformer = $imageTransformer;
        $mysqlImageRepo = new MySqlImageCharacteristicsRepository();
        $cacheRepo = new RedisImageCharacteristicsRepository();
        $uuidGenerator = new RamseyUuidGenerator();

        $this->saveImageCharacteristicsUseCase = new SaveImageCharacteristics($mysqlImageRepo, $cacheRepo, $uuidGenerator);
    }

    public function __invoke(Message $message)
    {
        switch ($message->getTransformation()) {
            case 'resizeToHeight500':
                $this->imageTransformer->resizeToHeight($message->getOriginalLocation(), 500, $message->getDestinationLocation());
                list($width, $height, $aux1, $aux2) = getimagesize($message->getDestinationLocation());
                $imageCharacteristics = ImageCharacteristics::generateNewImageCharacteristics($width, $height,$message->getTransformation(), $message->getDestinationLocation(), $message->getOriginalLocation());
                $this->saveImageCharacteristicsUseCase->__invoke($imageCharacteristics);
                break;
            case 'resizeToWidth500':
                $this->imageTransformer->resizeToWidth($message->getOriginalLocation(), 500, $message->getDestinationLocation());
                list($width, $height, $aux1, $aux2) = getimagesize($message->getDestinationLocation());
                $imageCharacteristics = ImageCharacteristics::generateNewImageCharacteristics($width, $height,$message->getTransformation(), $message->getDestinationLocation(), $message->getOriginalLocation());
                $this->saveImageCharacteristicsUseCase->__invoke($imageCharacteristics);
                break;
            case 'resizeToHeight200':
                $this->imageTransformer->resizeToHeight($message->getOriginalLocation(), 200, $message->getDestinationLocation());
                list($width, $height, $aux1, $aux2) = getimagesize($message->getDestinationLocation());
                $imageCharacteristics = ImageCharacteristics::generateNewImageCharacteristics($width, $height,$message->getTransformation(), $message->getDestinationLocation(), $message->getOriginalLocation());
                $this->saveImageCharacteristicsUseCase->__invoke($imageCharacteristics);
                break;
            case 'resizeToWidth200':
                $this->imageTransformer->resizeToWidth($message->getOriginalLocation(), 200, $message->getDestinationLocation());
                list($width, $height, $aux1, $aux2) = getimagesize($message->getDestinationLocation());
                $imageCharacteristics = ImageCharacteristics::generateNewImageCharacteristics($width, $height,$message->getTransformation(), $message->getDestinationLocation(), $message->getOriginalLocation());
                $this->saveImageCharacteristicsUseCase->__invoke($imageCharacteristics);
                break;
            case 'scaleImage50':
                $this->imageTransformer->scaleImage($message->getOriginalLocation(), 50, $message->getDestinationLocation());
                list($width, $height, $aux1, $aux2) = getimagesize($message->getDestinationLocation());
                $imageCharacteristics = ImageCharacteristics::generateNewImageCharacteristics($width, $height,$message->getTransformation(), $message->getDestinationLocation(), $message->getOriginalLocation());
                $this->saveImageCharacteristicsUseCase->__invoke($imageCharacteristics);
                break;
            case 'cropImage200_200':
                $this->imageTransformer->cropImage($message->getOriginalLocation(), 200, 200, $message->getDestinationLocation());
                list($width, $height, $aux1, $aux2) = getimagesize($message->getDestinationLocation());
                $imageCharacteristics = ImageCharacteristics::generateNewImageCharacteristics($width, $height,$message->getTransformation(), $message->getDestinationLocation(), $message->getOriginalLocation());
                $this->saveImageCharacteristicsUseCase->__invoke($imageCharacteristics);
                break;
            case 'gaussian_blur':
                $this->imageTransformer->addFilter($message->getOriginalLocation(), $message->getDestinationLocation());
                list($width, $height, $aux1, $aux2) = getimagesize($message->getDestinationLocation());
                $imageCharacteristics = ImageCharacteristics::generateNewImageCharacteristics($width, $height,$message->getTransformation(), $message->getDestinationLocation(), $message->getOriginalLocation());
                $this->saveImageCharacteristicsUseCase->__invoke($imageCharacteristics);
                break;
            default:
                break;
        }
    }

}