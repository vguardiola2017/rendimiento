<?php

namespace Viguamu\Performance\Application;


use Viguamu\Performance\Domain\Concurrency\ConcurrencyManager;

final class PublishMessageToQueueUseCase
{
    private $concurrencyManager;

    public function __construct(ConcurrencyManager $manager)
    {
        $this->concurrencyManager = $manager;
    }

    public function __invoke($queue, $message)
    {
        $this->concurrencyManager->publishMessage($queue, $message);
    }

}