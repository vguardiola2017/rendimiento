<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Application;

use Viguamu\Performance\Domain\Model\ImageDescriptions;
use Viguamu\Performance\Domain\Repository\ImageCharacteristicsRepository;
use Viguamu\Performance\Domain\Repository\ImageDescriptionsRepository;

class SaveEditedImageDataUseCase
{
    private $mysqlRepository;
    private $elasticRepository;
    private $redisRepository;

    public function __construct(ImageCharacteristicsRepository $mysqlRepository, ImageCharacteristicsRepository $redisRepository, ImageDescriptionsRepository $elasticRepository)
    {
        $this->mysqlRepository = $mysqlRepository;
        $this->elasticRepository = $elasticRepository;
        $this->redisRepository = $redisRepository;
    }

    public function __invoke(array $imageData)
    {
        $this->mysqlRepository->markImageAsEdited($imageData['id']);
        $this->redisRepository->markImageAsEdited($imageData['id']);

        $imageDescriptions = new ImageDescriptions($imageData['id'], $imageData['tags'], $imageData['description']);
        $this->elasticRepository->save($imageDescriptions);
    }

}