<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Application;

use Predis\Client;
use Viguamu\Performance\Domain\Repository\ImageCharacteristicsRepository;
use Viguamu\Performance\Domain\Repository\ImageDescriptionsRepository;

final class SearchEditedImagesUseCase
{
    private $cacheManager;
    private $elasticManager;
    private $mysqlManager;

    public function __construct(ImageCharacteristicsRepository $cacheManager, ImageDescriptionsRepository $elasticManager, ImageCharacteristicsRepository $mysqlManager)
    {
        $this->cacheManager   = $cacheManager;
        $this->elasticManager = $elasticManager;
        $this->mysqlManager   = $mysqlManager;
    }

    public function __invoke(string $textToSearch)
    {

        $redisClient = new Client();
        $redisResults = $redisClient->get($textToSearch);




        if (!$redisResults) {

            $results = [];
            $imagesDescriptions = $this->elasticManager->fullTextSearch($textToSearch);
            if(!$imagesDescriptions){return '';}

            foreach ($imagesDescriptions as $imageDescription) {
                $image = $this->mysqlManager->getImageCharacteristicsById($imageDescription['id']);

                $imageDescription['width'] = $image->getWidth();
                $imageDescription['height'] = $image->getHeight();
                $imageDescription['transformation'] = $image->getTransformation();
                $imageDescription['location'] = $image->getLocation();
                $imageDescription['original_location'] = $image->getOriginalImageLocation();
                $imageDescription['name'] = explode('/', $imageDescription['location'])[1];

                $results[] = $imageDescription;
            }

            $redisClient->set($textToSearch, json_encode($results));

        } else {
            $results = json_decode($redisResults, true);
        }

        return $results;
    }
}