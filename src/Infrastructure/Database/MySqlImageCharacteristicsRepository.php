<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Infrastructure\Database;

use Viguamu\Performance\Domain\Model\ImageCharacteristics;
use Viguamu\Performance\Domain\Repository\ImageCharacteristicsRepository;

final class MySqlImageCharacteristicsRepository implements ImageCharacteristicsRepository
{
    private $connection;

    public function __construct()
    {
        $host     = "localhost";
        $db_name  = "image_data";
        $username = "root";
        $password = "root";

        try {
            $this->connection = new \PDO(
                "mysql:host={$host};dbname={$db_name}",
                $username, $password
            );
            $this->connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        } catch (\PDOException $exception) {
            echo "Error: " . $exception->getMessage();
        }
    }

    public function save(ImageCharacteristics $imageData): void
    {
        $query     =
            "INSERT INTO image_data (id, width, height, transformation, location, original_location) VALUES(:id, :width, :height, :transformation, :location, :original_location)";
        $statement = $this->connection->prepare($query);

        $id                = $imageData->getId();
        $width             = $imageData->getWidth();
        $height            = $imageData->getHeight();
        $transformation    = $imageData->getTransformation();
        $location          = $imageData->getLocation();
        $original_location = $imageData->getOriginalImageLocation();

        $statement->bindParam(':id', $id);
        $statement->bindParam(':width', $width);
        $statement->bindParam(':height', $height);
        $statement->bindParam(':transformation', $transformation);
        $statement->bindParam(':location', $location);
        $statement->bindParam(':original_location', $original_location);

        $statement->execute();
    }

    public function getUneditedImages(): array
    {
        $query     =
            'SELECT id, width, height, transformation, location, original_location FROM image_data WHERE edited = 0';
        $statement = $this->connection->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll();

        return $result;
    }

    public function markImageAsEdited(string $id): void
    {
        $query     = 'UPDATE image_data SET edited = 1 WHERE id = :id';
        $statement = $this->connection->prepare($query);

        $statement->bindParam(':id', $id);
        $statement->execute();
    }

    public function getImageCharacteristicsById(string $id): ImageCharacteristics
    {
        $query     =
            'SELECT id, width, height, transformation, location, original_location FROM image_data WHERE id = :id';
        $statement = $this->connection->prepare($query);
        $statement->bindParam(':id', $id);
        $statement->execute();
        $result = $statement->fetch();

        $imageCharacteristics = new ImageCharacteristics(
            $result['id'],
            intval($result['width']),
            intval($result['height']),
            $result['transformation'],
            $result['location'],
            $result['original_location']
        );


        return $imageCharacteristics;
    }
}