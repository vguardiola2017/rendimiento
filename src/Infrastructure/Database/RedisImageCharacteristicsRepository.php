<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Infrastructure\Database;

use Predis\Client;
use Viguamu\Performance\Domain\Model\ImageCharacteristics;
use Viguamu\Performance\Domain\Repository\ImageCharacteristicsRepository;

final class RedisImageCharacteristicsRepository implements ImageCharacteristicsRepository
{
    private $redisClient;

    public function __construct()
    {
        $this->redisClient = new Client();
    }

    public function save(ImageCharacteristics $image): void
    {
        $dataToSave = [
            'id'                => $image->getId(),
            'width'             => $image->getWidth(),
            'height'            => $image->getHeight(),
            'transformation'    => $image->getTransformation(),
            'location'          => $image->getLocation(),
            'original_location' => $image->getOriginalImageLocation(),
            'edited'            => 0,
        ];

        $this->redisClient->hmset($image->getId(), $dataToSave);
        $this->redisClient->zadd('images.edited', [$image->getId() => 0]);
    }

    public function getUneditedImages(): array
    {
        $result         = [];
        $uneditedImages = $this->redisClient->zrangebyscore('images.edited', 0, 0);
        foreach ($uneditedImages as $uneditedImage) {
            $result[] = $this->redisClient->hgetall($uneditedImage);
        }

        return $result;
    }

    public function markImageAsEdited(string $id): void
    {
        $this->redisClient->hmset($id, ['edited' => 1]);
        $this->redisClient->zadd('images.edited', [$id => 1]);
    }

    public function getImageCharacteristicsById(string $id): ImageCharacteristics
    {
        $result = $this->redisClient->hgetall($id);

        return new ImageCharacteristics(
            $result['id'],
            intval($result['width']),
            intval($result['height']),
            $result['transformation'],
            $result['location'],
            $result['original_location']
        );
    }
}