<?php

declare(strict_types = 1);

namespace Viguamu\Performance\Infrastructure\Database;

use Elasticsearch\ClientBuilder;
use Viguamu\Performance\Domain\Model\ImageDescriptions;
use Viguamu\Performance\Domain\Repository\ImageDescriptionsRepository;

final class ElasticImageDescriptionsRepository implements ImageDescriptionsRepository
{
    private $client;

    public function __construct()
    {
        $this->client = ClientBuilder::create()->build();
    }

    public function save(ImageDescriptions $imageDescriptions): void
    {
        $params = [
            'index' => 'image_data',
            'type'  => '_doc',
            'id'    => $imageDescriptions->getId(),
            'body'  => [
                'description' => $imageDescriptions->getDescription(),
                'tags'        => $imageDescriptions->getTags(),
            ],
        ];

        $this->client->index($params);
    }

    public function fullTextSearch(string $textToSearch): array
    {
        $params       = [
            'index' => 'image_data',
            'type'  => '_doc',
            'body'  => [
                "query" => [
                    "bool" => [
                        "should" => [
                            ["wildcard" => ["description" => "*" . $textToSearch . "*"]],
                            ["wildcard" => ["tags" => "*" . $textToSearch . "*"]],
                        ],
                    ],
                ],
            ],
        ];
        $rawDocuments = $this->client->search($params);
        $documents    = $rawDocuments['hits']['hits'];

        $images = [];
        foreach ($documents as $document) {
            $image                = [];
            $image['id']          = $document['_id'];
            $image['description'] = $document['_source']['description'];
            $image['tags']        = $document['_source']['tags'];
            $images[]             = $image;
        }

        return $images;
    }
}