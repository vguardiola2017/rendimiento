<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Infraestructure\Uuid;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Viguamu\Performance\Domain\Uuid\UuidGenerator;

final class RamseyUuidGenerator implements UuidGenerator
{
    public function generateUuid(): string
    {
        try {
            $uuid4 = Uuid::uuid4();
            return $uuid4->toString();

        } catch (UnsatisfiedDependencyException $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
    }
}