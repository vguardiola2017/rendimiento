<?php
declare(strict_types = 1);

namespace Viguamu\Performance\Infrastructure\ImageTransformer;

use Gumlet\ImageResize;
use Viguamu\Performance\Domain\ImageTransformer\ImageTransformer;

final class GumletImageTransformer implements ImageTransformer
{
    public function resizeToHeight(string $imageSource, int $height, string $imageDestination): void
    {
        $image = new ImageResize($imageSource);
        $image->resizeToHeight($height);
        $image->save($imageDestination);
    }

    public function resizeToWidth(string $imageSource, int $width, string $imageDestination): void
    {
        $image = new ImageResize($imageSource);
        $image->resizeToWidth($width);
        $image->save($imageDestination);
    }

    public function cropImage(string $imageSource, int $height, int $width, string $imageDestination): void
    {
        $image = new ImageResize($imageSource);
        $image->crop($width, $height);
        $image->save($imageDestination);
    }

    public function addFilter(string $imageSource, string $imageDestination): void
    {
        $image = new ImageResize($imageSource);
        $image->addFilter(function ($imageDestination) {
            imagefilter($imageDestination, IMG_FILTER_GAUSSIAN_BLUR);
        });
        $image->save($imageDestination);
    }

    public function scaleImage(string $imageSource, int $scale, string $imageDestination): void
    {
        $image = new ImageResize($imageSource);
        $image->scale($scale);
        $image->save($imageDestination);
    }
}