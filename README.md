## Preparar el entorno

Para que todo funcione correctamente tendremos que conectarnos a la consola de mysql y ejecutar las querys que se pueden encontrar en el archivo **database.sql**.

Posteriormente haremos un **php composer install** para instalar todas las dependencias del proyecto.
```
php composer install
```

Cabe señalar que todos los pasos necesarios para probar la aplicación o prepararla para su correcto funcionamiento requieren de un **vagrant up** para encender la máquina virtual.
```
cd /mpwar-performance-vm/vagrant
vagrant up
```

Una vez levantada la máquina podremos acceder a nuestra aplicación web en la siguiente URL: http://192.168.33.50/index.php


## Casos de uso implementados

- Subida de imágenes asíncrona.
- Listar imágenes no editadas.
- Búsqueda de imágenes por tags y descripción (Una sola palabra, imágenes que ya hayan sido editadas).
- Edición de las imágenes una a una (tags y descripción, la aplicación no está preparada para editar los demás campos).
- Transformación automática de las imágenes.

## RabbitMQ

Las transformacions se han realizado con **consumers** de RabbitMQ para poder hacerlo de forma asíncrona. 

**Transformaciones realizadas:**

-  Resize en altura a 500px.
-  Resize en anchura a 500px.
-  Resize en altura a 200px.
-  Resize en anchura a 200px.
-  Escalado a un 50% del tamaño original.
-  Crop de a 200px x 200px.
-  Filtro Gaussian Blur.

Tras procesar la imágenes, la información se guarda en MySql y en Redis, posteriormente podremos editarlas y así guardar la descripción y los tags en ElasticSearch para poder buscar después.

Para ejecutar el consumer tendremos que lanzar este comando en la consola:
```
php consumer.php
```

## ElasticSearch

Después de editar las imágenes para añadir la descripción y los tags, la información se guarda en elastic para poder buscar imágenes por palabras contenidas en la descripción o en los tags.

## Blackfire

Tras hacer muchas pruebas no hemos conseguido notar una gran diferencia a la hora de acceder a Redis o a MySQL (Haciendo el mismo número de consultas no conseguimos ninguna mejora)

Como podemos ver en la captura, al hacer las peticiones de cada una de las imágenes a Redis, las peticiones tardan más. Esto es debido a que al acceder a la caché y recuperar los objetos guardados en ella hay que decodificar las respuestas. Esto aumenta el tiempo de uso del procesador de 10ms a 17.4ms.

![alt text](INFORME/ComparacionRedisVSMysql.png)

Tras constatar que en nuestro caso acceder a Redis y no a MySQL no ayuda a mejorar el rendimiento, vamos a seguir analizando la aplicación probando distintos cambios en el código.

![alt text](INFORME/SinCacheRespuestaFinal-ComposerNOOptimized.png)

En esta captura hemos desactivado la optimización de composer **"optimize-autoloader": false** y hacemos las peticiones contra MySQL. Este será nuestro peor caso a la hora de comprobar la mejora que conseguimos tras probar varias optimizaciones.

![alt text](INFORME/CacheRespuestaFinal-ComposerNOOptimized.png)

Ahora decidimos cachear la respuesta a la búsqueda que hacemos en Elastic. De normal se busca en elastic (1 petición) y nos devuelve los IDs de las imágenes a buscar en Redis o MySQL, en el caso que estamos analizando ahora, tras recibir la respuesta con todas la peticiones hechas nos guardamos en caché el resultado.

Podemos ver que la diferencia empieza a notarse, hemos conseguido bajar mucho el número de peticiones.

![alt text](INFORME/CacheRespuestaFinal-ComposerSIOptimized.png)

Finalmente activamos la optimización de composer **"optimize-autoloader": true** y vemos que la diferencia es enorme.

![alt text](INFORME/SinCacheFinal-NoOptimizedVSMAX3.png)

Hemos conseguido bajar el tiempo de respuesta en un 87% gracias a la reducción del 97% del tiempo de acceso a datos y una reducción del tiempo de CPU del 78%.

![alt text](INFORME/SinCacheFinal-NoOptimizedVSMAX.png)

Toda esta mejora la hemos conseguido gracias la optimización de composer, la petición que ya no hacemos a ElasticSearch y sobre todo a las peticiones que no hacemos a MySQL o Redis.

![alt text](INFORME/SinCacheFinal-NoOptimizedVSMAX2.png)

En nuestro análisis la busqueda de 'a' nos retornaba 10 imágenes y por tanto con la caché activada en ese caso conseguíamos ahorrarnos 10 peticiones.




