<?php

require_once 'autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Viguamu\Performance\Application\ConsumeMessageUseCase;
use Viguamu\Performance\Domain\Model\Message;
use Viguamu\Performance\Infrastructure\ImageTransformer\GumletImageTransformer;

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare('transformations', false, true, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function($msg){

    $imageTransformer = new GumletImageTransformer();
    $consumerUseCase = new ConsumeMessageUseCase($imageTransformer);

    $message = new Message($msg->body);
    $consumerUseCase($message);
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

$channel->basic_qos(null, 1, null);
$channel->basic_consume('transformations', '', false, false, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();